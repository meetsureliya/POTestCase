package com.onepay.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;


@Entity
@Table(name = "PO_Master")
public class POCreationDTO {

	@Id
	int po_id;
	Date po_date;
	int po_status;
	String supp_id;
	String created_by;
	Date created_on;
	String approved_by;
	Date approved_on;
	Date rodt;
	Double order_value;
	Double cgst;
	Double sgst;
	Double total_order_value;
	Date exp_del_date;
	Date last_updated_on;
	String last_updated_by;
	
	public int getPo_id() {
		return po_id;
	}
	public void setPo_id(int po_id) {
		this.po_id = po_id;
	}
	public Date getPo_date() {
		return po_date;
	}
	public void setPo_date(Date po_date) {
		this.po_date = po_date;
	}
	public int getPo_status() {
		return po_status;
	}
	public void setPo_status(int po_status) {
		this.po_status = po_status;
	}
	public String getSupp_id() {
		return supp_id;
	}
	public void setSupp_id(String supp_id) {
		this.supp_id = supp_id;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public Date getCreated_on() {
		return created_on;
	}
	public void setCreated_on(Date created_on) {
		this.created_on = created_on;
	}
	public String getApproved_by() {
		return approved_by;
	}
	public void setApproved_by(String approved_by) {
		this.approved_by = approved_by;
	}
	public Date getApproved_on() {
		return approved_on;
	}
	public void setApproved_on(Date approved_on) {
		this.approved_on = approved_on;
	}
	public Date getRodt() {
		return rodt;
	}
	public void setRodt(Date rodt) {
		this.rodt = rodt;
	}
	public Double getOrder_value() {
		return order_value;
	}
	public void setOrder_value(Double order_value) {
		this.order_value = order_value;
	}
	public Double getCgst() {
		return cgst;
	}
	public void setCgst(Double cgst) {
		this.cgst = cgst;
	}
	public Double getSgst() {
		return sgst;
	}
	public void setSgst(Double sgst) {
		this.sgst = sgst;
	}
	public Double getTotal_order_value() {
		return total_order_value;
	}
	public void setTotal_order_value(Double total_order_value) {
		this.total_order_value = total_order_value;
	}
	public Date getExp_del_date() {
		return exp_del_date;
	}
	public void setExp_del_date(Date exp_del_date) {
		this.exp_del_date = exp_del_date;
	}
	public Date getLast_updated_on() {
		return last_updated_on;
	}
	public void setLast_updated_on(Date last_updated_on) {
		this.last_updated_on = last_updated_on;
	}
	public String getLast_updated_by() {
		return last_updated_by;
	}
	public void setLast_updated_by(String last_updated_by) {
		this.last_updated_by = last_updated_by;
	}
	

}
