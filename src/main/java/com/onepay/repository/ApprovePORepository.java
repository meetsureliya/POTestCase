package com.onepay.repository;

import java.io.Serializable;
import java.sql.Date;

import javax.transaction.Transactional;
import java.util.ArrayList;


import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.onepay.model.POCreationDTO;
@Repository
public interface ApprovePORepository extends CrudRepository<POCreationDTO, Serializable>  {
	
	@Modifying(clearAutomatically = true)
    @Transactional
    @Query("UPDATE POCreationDTO c SET c.po_status = ?1,c.approved_by=?2,c.approved_on=?3,c.last_updated_on=?4,c.last_updated_by=?5 WHERE c.po_id = ?6")
    int updateApproveStatus(int status,String approvedby ,java.util.Date poDate ,java.util.Date poDate2 ,String lastUpdatedBy ,int po_id );
	
	@Query("FROM POCreationDTO c  where c.po_id = ?1")
	ArrayList<POCreationDTO> findByPoId(int po_id);

}
