package com.onepay.repository;

import java.io.Serializable;
import java.util.ArrayList;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.onepay.model.POCreationDTO;
@Repository
public interface CreatePORepository extends CrudRepository<POCreationDTO, Serializable> {
	
	 @Query("FROM POCreationDTO p  where p.po_id  = ?1")
     ArrayList<POCreationDTO> findByPoId(int  po_id);
 }
