package com.onepay.repository;

import java.io.Serializable;
import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.onepay.model.POCreationDTO;

@Repository
public interface RejectPORepository  extends CrudRepository<POCreationDTO, Serializable> {
	
	@Modifying(clearAutomatically = true)
    @Transactional
    @Query("UPDATE POCreationDTO c SET c.po_status = ?1,c.last_updated_on=?2,c.last_updated_by=?3 WHERE c.po_id = ?4")
    int updateRejectStatus(int status,java.util.Date poDate2 ,String lastUpdatedBy ,int po_id );
	
	@Query("FROM POCreationDTO c  where c.po_id = ?1")
	ArrayList<POCreationDTO> findByPoId(int po_id);


}
