package com.onepay.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.onepay.model.POCreationDTO;
import com.onepay.repository.CreatePORepository;
import com.onepay.service.POCreationService;

@RestController
public class POCreationController {
 
	@Autowired
	CreatePORepository createPORepository;
	
	@Autowired
	POCreationService pOCreationService;

	@PostMapping("/CreatePO")
	public String POCreation(@RequestParam("date") String date, @RequestParam("supplierId") String supplierId,
			@RequestParam("tagClassId") String tagClassId, @RequestParam("orderQuantity") String orderQuantity,
			@RequestParam("unitPrice") String unitPrice) throws ParseException {

 		// createPORepository.deleteAll();
  		 
 		 if (isThisDateValid(date, "dd/MM/yyyy") != true) {
			return "Date format is not Correct!";
 		 }
 		 Date fromdate = new SimpleDateFormat("dd/MM/yyyy").parse(date);
 		 Date curdate = new Date();

 		 if (fromdate.compareTo(curdate) > 0) {
			return "Date is too late!";
 		 }
		// if (fromdate.compareTo(curdate) < 0) {
		// map.put("Date is too early!", null);
		// return map;

		// }

		if (supplierId.contains(",")) {

					List<String> list1 = Arrays.asList(supplierId.split(","));
					System.out.println(list1);
					if (list1 == null || list1.isEmpty()) {
						return "supplierId is not null!";
					}
					List<String> list2 = Arrays.asList(tagClassId.split(","));
					System.out.println(list2);
					if (list2 == null || list2.isEmpty()) {
						return "tagClassId is not null!";
					}
					List<String> list3 = Arrays.asList(orderQuantity.split(","));
					System.out.println(list3);
					if (list3 == null || list3.isEmpty()) {
						return "orderQuantity is not null!";
					}
					if (orderQuantity.length() > 15) {
						return "Order qty should be maximum 15 character.";
					}
					List<String> list4 = Arrays.asList(unitPrice.split(","));
					System.out.println(list4);
					if (list4 == null || list4.isEmpty()) {
						return "unitPrice is not null!";
					}
					if (unitPrice.length() > 15) {
						return "Unit price should be maximum 15 character.";
					}
					
					String poStatus = pOCreationService.createMultiplePO(fromdate,list1,list2,list3,list4);
					
					return poStatus;
					
					
		} else {
					if (fromdate.compareTo(curdate) > 0) {
						return "Date is too late!";
					}
					// if (fromdate.compareTo(curdate) < 0) {
					// map.put("Date is too early!", null);
					// return map;
		
					// }
		
					if (supplierId == null || supplierId.isEmpty()) {
						return "supplierId is not null!";
					}
		
					if (tagClassId == null || tagClassId.isEmpty()) {
						return "tagClassId is not null!";
					}
		
					if (orderQuantity == null || orderQuantity.isEmpty()) {
						return "orderQuantity is not null!";
					}
					if (orderQuantity.length() > 15) {
						return "Order qty should be maximum 15 character.";
					}
		
					if (unitPrice == null || unitPrice.isEmpty()) {
						return "unitPrice is not null!";
					}
					if (unitPrice.length() > 15) {
						return "Unit price should be maximum 15 character.";
					}
					
					String poStatus = pOCreationService.createSinglePO(fromdate,supplierId,tagClassId,orderQuantity,unitPrice);
					
					return poStatus;
				}
 	}

	
	public boolean isThisDateValid(String dateToValidate, String dateFromat) {

		if (dateToValidate == null) {
			return false;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		sdf.setLenient(false);
		try {
			// if not valid, it will throw ParseException
			Date date = sdf.parse(dateToValidate);
			System.out.println(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
 	}
