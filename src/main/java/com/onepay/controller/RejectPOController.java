
package com.onepay.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.onepay.service.RejectPOService;

@RestController
public class RejectPOController {

	@Autowired
	RejectPOService rejectPOService;

	@PostMapping("/RejectPO")
	public String POCreation(@RequestParam("po_id") String po_id,@RequestParam("RejectBy") String rejectBy ) throws ParseException {

		Date poDate = new Date();

		boolean approveStatus = rejectPOService.rejectPO(po_id.trim(), poDate,rejectBy.trim());
		if (approveStatus == true) {
			return "Rejected";
		} else {
			return "Not Rejected ! PO is not there for Id " + po_id + "";
		}

	}

	public boolean isThisDateValid(String dateToValidate, String dateFromat) {

		if (dateToValidate == null) {
			return false;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		sdf.setLenient(false);
		try {
			// if not valid, it will throw ParseException
			Date date = sdf.parse(dateToValidate);
			System.out.println(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
