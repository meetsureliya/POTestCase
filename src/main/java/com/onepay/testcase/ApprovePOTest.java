package com.onepay.testcase;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.junit4.SpringRunner;

import com.onepay.model.POCreationDTO;
import com.onepay.service.ApprovePOService;

import io.restassured.response.Response;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;

 
@RunWith(SpringRunner.class)
@SpringBootTest
class ApprovePOTest {
	
	int poId = 0;
	String WrongpoId = "12";
 
	@Autowired
	ApprovePOService approvePOService; 
	 
	@BeforeEach
	public void initEach() {
		poId = approvePOService.getData();
		System.out.println("**********GetRandomTestPOData*****poId.."+poId+"*******");
		assertNotEquals(0, poId);
		
	}
 
	//TestCase 1 : Check PO Approved or Not in DB
		@Test
		public void CheckPoApproved() {
			
			Response response = given().contentType("application/json").accept("application/json")
					.queryParam("po_id", poId).queryParam("approvedBy", "Admin").when()
					.post("http://localhost:8081" + "/ApprovePO").then().contentType("").extract().response();

			// Now let us print the body of the message to see what response
			// we have received from the server
			System.out.println(response.getBody().asString());
			String resp = response.getBody().asString();
			
			assertEquals("Success, If Approved Msg. is Matched", "Approved", resp);
			
			ArrayList<POCreationDTO> poDbList = approvePOService.getPOData(String.valueOf(poId));
	 
			assertEquals("POid Same then Data Inserted Succesfully", poId, poDbList.get(0).getPo_id());
		}
	
	//TestCase 2 : Check PO Approved Status is Updated or Not
	@Test
	public void CheckPoApprovedStatus() {
		
		Response response = given().contentType("application/json").accept("application/json")
				.queryParam("po_id", poId).queryParam("approvedBy", "Admin").when()
				.post("http://localhost:8081" + "/ApprovePO").then().contentType("").extract().response();

		// Now let us print the body of the message to see what response
		// we have received from the server
		System.out.println(response.getBody().asString());
		String resp = response.getBody().asString();
		
		assertEquals("Success, If Approved Msg. is Matched", "Approved", resp);

		ArrayList<POCreationDTO> poDbList = approvePOService.getPOData(String.valueOf(poId));
       
		//Default Status is 0, If Approved Status changed to 1 
		assertEquals("Status 1, Data Updated Successfully ", 1, poDbList.get(0).getPo_status());
	}
	
	//TestCase 3 : Check Approved By is Updated or Not
	@Test
	public void CheckPoApprovedBy() {
		
		Response response = given().contentType("application/json").accept("application/json")
				.queryParam("po_id",poId).queryParam("approvedBy", "Admin").when()
				.post("http://localhost:8081" + "/ApprovePO").then().contentType("").extract().response();

		// Now let us print the body of the message to see what response
		// we have received from the server
		System.out.println(response.getBody().asString());
		String resp = response.getBody().asString();
		
		assertEquals("Success, If Approved Msg. is Matched", "Approved", resp);
  
		ArrayList<POCreationDTO> poDbList = approvePOService.getPOData(String.valueOf(poId));
 
		assertEquals("POid Same then Data Inserted Succesfully", "Admin", poDbList.get(0).getApproved_by() );
	}
	
	//TestCase 4 : Check PO LastUpdatedBy is Updated or Not
	@Test
	public void CheckPoApprovedLastUpdatedBy() {
		
		Response response = given().contentType("application/json").accept("application/json")
				.queryParam("po_id",poId ).queryParam("approvedBy", "Admin").when()
				.post("http://localhost:8081" + "/ApprovePO").then().contentType("").extract().response();

		// Now let us print the body of the message to see what response
		// we have received from the server
		System.out.println(response.getBody().asString());
		String resp = response.getBody().asString();
		
		assertEquals("Success, If Approved Msg. is Matched", "Approved", resp);

		ArrayList<POCreationDTO> poDbList = approvePOService.getPOData(String.valueOf(poId));
       
		//Default Status is 0, If Approved Status changed to 1 
		assertEquals("LastUpdatedBY Admin, Data Updated Successfully ", "Admin", poDbList.get(0).getLast_updated_by());
	}
 
	//TestCase 5 : Check For Wrong POId, PO is Approved or Not
	@Test
	public void CheckPoApprovedByWrongId() {
		
		Response response = given().contentType("application/json").accept("application/json")
				.queryParam("po_id", WrongpoId).queryParam("approvedBy", "Admin").when()
				.post("http://localhost:8081" + "/ApprovePO").then().contentType("").extract().response();

		// Now let us print the body of the message to see what response
		// we have received from the server
		System.out.println(response.getBody().asString());
		String resp = response.getBody().asString();
 
		assertEquals("Success, If Not Approved Msg. is Matched", "Not Approved ! PO is not there for Id "+WrongpoId, resp);

		ArrayList<POCreationDTO> poDbList = approvePOService.getPOData(WrongpoId);
		 
		assertNull(poDbList); 
 
	}
 
	

}
