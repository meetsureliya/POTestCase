package com.onepay.testcase;

import static org.junit.Assert.assertEquals;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.test.context.junit4.SpringRunner;
import org.junit.runner.RunWith;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.onepay.model.POCreationDTO;
import com.onepay.service.POCreationService;

import static io.restassured.RestAssured.given;

import io.restassured.response.Response;

@RunWith(SpringRunner.class)
@SpringBootTest
public class POCreationTest {

	@Autowired
	POCreationService pOCreationService;

	@BeforeEach
	public void initEach() {
		System.out.println("**********ResetData************");
		pOCreationService.ResetData();
	}

	// TestCase1 -- test Data is being Inserted in po_master
	@Test
	public void POCreationTestSinglePO() {

		// poCreationController.ResetData();

		Response response = given().contentType("application/json").accept("application/json")
				.queryParam("date", "05/05/2020").queryParam("supplierId", "HDFC").queryParam("tagClassId", "HDFC")
				.queryParam("orderQuantity", "2").queryParam("unitPrice", "3").when()
				.post("http://localhost:8081" + "/CreatePO").then().contentType("").extract().response();

		// Now let us print the body of the message to see what response
		// we have received from the server
		System.out.println("TestCase1 Response : "+response.getBody().asString());
		String resp = response.getBody().asString();
		int resp1 = Integer.valueOf(resp.substring(12, resp.length()));

		int poIdDb = pOCreationService.getData(resp1);

		assertEquals("POid Same then Data Inserted Succesfully", poIdDb, resp1);
	}

	// TestCase2 -- // test wrong Future Date throws error/Or Allowed
	@Test
	public void POCreationTestWrongDate() {

		Response response = given().contentType("application/json").accept("application/json")
				.queryParam("date", "05/05/20201").queryParam("supplierId", "HDFC").queryParam("tagClassId", "HDFC")
				.queryParam("orderQuantity", "2").queryParam("unitPrice", "3").when()
				.post("http://localhost:8081" + "/CreatePO").then().contentType("").extract().response();

		// Now let us print the body of the message to see what response
		// we have received from the server
		System.out.println("TestCase2 Response : "+response.getBody().asString());
		String resp = response.getBody().asString();

		assertEquals("Success,Date is too late", "Date is too late!", resp);
	}

	// TestCase3 -- // check EmptySuppId Allow OR Not
	@Test
	public void POCreationTestEmptySuppId() {

		// poCreationController.ResetData();

		Response response = given().contentType("application/json").accept("application/json")
				.queryParam("date", "05/05/2020").queryParam("supplierId", "").queryParam("tagClassId", "HDFC")
				.queryParam("orderQuantity", "2").queryParam("unitPrice", "3").when()
				.post("http://localhost:8081" + "/CreatePO").then().contentType("").extract().response();

		// Now let us print the body of the message to see what response
		// we have received from the server
		System.out.println("TestCase3 Response : "+response.getBody().asString());
		String resp = response.getBody().asString();

		assertEquals("Suceess, If Error match", "supplierId is not null!", resp);
	}

	// TestCase4 -- check EmptytagClasId Allow OR Not
	@Test
	public void POCreationTestEmptytagClassId() {

		// poCreationController.ResetData();

		Response response = given().contentType("application/json").accept("application/json")
				.queryParam("date", "05/05/2020").queryParam("supplierId", "HDFC").queryParam("tagClassId", "")
				.queryParam("orderQuantity", "2").queryParam("unitPrice", "3").when()
				.post("http://localhost:8081" + "/CreatePO").then().contentType("").extract().response();

		// Now let us print the body of the message to see what response
		// we have received from the server
		System.out.println("TestCase4 Response : "+response.getBody().asString());
		String resp = response.getBody().asString();

		assertEquals("Suceess, If Error match", "tagClassId is not null!", resp);
	}

	// TestCase5 -- check EmptyOrderQuantity Allow OR Not
	@Test
	public void POCreationTestEmptyOrderQuantity() {

		// poCreationController.ResetData();

		Response response = given().contentType("application/json").accept("application/json")
				.queryParam("date", "05/05/2020").queryParam("supplierId", "HDFC").queryParam("tagClassId", "HDFC")
				.queryParam("orderQuantity", "").queryParam("unitPrice", "3").when()
				.post("http://localhost:8081" + "/CreatePO").then().contentType("").extract().response();

		// Now let us print the body of the message to see what response
		// we have received from the server
		System.out.println("TestCase5 Response : "+response.getBody().asString());
		String resp = response.getBody().asString();

		assertEquals("Suceess, If Error match", "orderQuantity is not null!", resp);
	}

	// TestCase6 -- check EmptyUnitPrice Allow OR Not
	@Test
	public void POCreationTestEmptyUnitPrice() {

		Response response = given().contentType("application/json").accept("application/json")
				.queryParam("date", "05/05/2020").queryParam("supplierId", "HDFC").queryParam("tagClassId", "HDFC")
				.queryParam("orderQuantity", "2").queryParam("unitPrice", "").when()
				.post("http://localhost:8081" + "/CreatePO").then().contentType("").extract().response();

		// Now let us print the body of the message to see what response
		// we have received from the server
		System.out.println("TestCase6 Response : "+response.getBody().asString());
		String resp = response.getBody().asString();

		assertEquals("Suceess, If Error match", "unitPrice is not null!", resp);
	}

	// TestCase7 -- check Max 15 orderQtyMaxSize Allow OR Not
	@Test
	public void POCreationTestOrderQtyMaxSize() {

		Response response = given().contentType("application/json").accept("application/json")
				.queryParam("date", "05/05/2020").queryParam("supplierId", "HDFC").queryParam("tagClassId", "HDFC")
				.queryParam("orderQuantity", "2111111111111111").queryParam("unitPrice", "3").when()
				.post("http://localhost:8081" + "/CreatePO").then().contentType("").extract().response();

		// Now let us print the body of the message to see what response
		// we have received from the server
		System.out.println("TestCase7 Response : "+response.getBody().asString());
		String resp = response.getBody().asString();

		assertEquals("Suceess, If Error match", "Order qty should be maximum 15 character.", resp);
	}

	// TestCase8 -- check Max 15 UnitPrcMaxSize Allow OR Not
	@Test
	public void POCreationTestUnitPrcMaxSize() {

		Response response = given().contentType("application/json").accept("application/json")
				.queryParam("date", "05/05/2020").queryParam("supplierId", "HDFC").queryParam("tagClassId", "HDFC")
				.queryParam("orderQuantity", "2").queryParam("unitPrice", "3111111111111111").when()
				.post("http://localhost:8081" + "/CreatePO").then().contentType("").extract().response();

		// Now let us print the body of the message to see what response
		// we have received from the server
		System.out.println("TestCase8 Response : "+response.getBody().asString());
		String resp = response.getBody().asString();

		assertEquals("Suceess, If Error match", "Unit price should be maximum 15 character.", resp);
	}

	// TestCase9 -- check TotalQtyShouldMatch with given data
	@Test
	public void POCreationTest_MultiplePo_TtlQty() {

		Response response = given().contentType("application/json").accept("application/json")
				.queryParam("date", "05/05/2020").queryParam("supplierId", "HDFC,HDFC1")
				.queryParam("tagClassId", "HDFC,HDFC1").queryParam("orderQuantity", "2,3")
				.queryParam("unitPrice", "4,5").when().post("http://localhost:8081" + "/CreatePO").then()
				.contentType("").extract().response();

		// Now let us print the body of the message to see what response
		// we have received from the server
		String resp = response.getBody().asString();
		System.out.println("TestCase9 Response : "+resp);

		Double poSizeDb = pOCreationService.getPOttlQty();

		assertEquals("Suceess, If TotalQty match", "23.0", poSizeDb.toString());
	}

	// TestCase10 -- check Multiple Data Should Insert Or Not
	@Test
	public void POCreationTestMultiplePo_DataInsert() {

		Response response = given().contentType("application/json").accept("application/json")
				.queryParam("date", "05/05/2020").queryParam("supplierId", "HDFC,HDFC1")
				.queryParam("tagClassId", "HDFC,HDFC1").queryParam("orderQuantity", "2,3")
				.queryParam("unitPrice", "4,5").when().post("http://localhost:8081" + "/CreatePO").then()
				.contentType("").extract().response();

		// Now let us print the body of the message to see what response
		// we have received from the server
		String resp = response.getBody().asString();
		System.out.println("TestCase10 Response : "+resp);

		int DbPOSize = pOCreationService.findAllData();

		assertEquals("Suceess,If SIZE Match", 2, DbPOSize);
	}

	// TestCase11 -- check POId inserted same As Given Should Insert Or Not
	@Test
	public void POCreationTestMultiplePo_PoIdMatch() {

		Response response = given().contentType("application/json").accept("application/json")
				.queryParam("date", "05/05/2020").queryParam("supplierId", "SP200411274,SP200374671")
				.queryParam("tagClassId", "VC4,VC5").queryParam("orderQuantity", "2,3").queryParam("unitPrice", "4,5")
				.when().post("http://localhost:8081" + "/CreatePO").then().contentType("").extract().response();

		// Now let us print the body of the message to see what response
		// we have received from the server
		System.out.println("TestCase11 Response : "+response.getBody().asString());
		String resp = response.getBody().asString();
		String PoId = resp.substring(resp.indexOf("[") + 1, resp.indexOf("]"));

		List<String> PoIdInp = Arrays.asList(PoId.split(","));
		List<Integer> PoIdIn = new ArrayList<Integer>();

		for (int i = 0; i < PoIdInp.size(); i++) {
			PoIdIn.add(Integer.parseInt(PoIdInp.get(i).trim()));
		}
		Collections.sort(PoIdIn);
		System.out.println("PoIdIn..................." + ", " + PoIdIn + PoIdIn.get(0).toString().trim() + ", "
				+ PoIdIn.get(1).toString().trim());
		List<Integer> DbPOS = pOCreationService.findPoId();
		Collections.sort(DbPOS);
		System.out.println("DbPOS..................." + ", " + DbPOS + DbPOS.get(0).toString().trim() + ", "
				+ DbPOS.get(1).toString().trim());

		for (int i = 0; i < PoIdIn.size(); i++) {
			assertEquals("Suceess,If PoId Match", PoIdIn.get(i).toString().trim(), DbPOS.get(i).toString().trim());
		}
	}

	// TestCase12 -- check Order_value inserted same As Given Should Insert Or Not
	@Test
	public void POCreationTest_MultiplePo_OrderValMatch() {

		Response response = given().contentType("application/json").accept("application/json")
				.queryParam("date", "05/05/2020").queryParam("supplierId", "HDFC,HDFC1")
				.queryParam("tagClassId", "HDFC,HDFC1").queryParam("orderQuantity", "2,3")
				.queryParam("unitPrice", "5,4").when().post("http://localhost:8081" + "/CreatePO").then()
				.contentType("").extract().response();

		// Now let us print the body of the message to see what response
		// we have received from the server
		String resp = response.getBody().asString();
		System.out.println("TestCase12 Response : "+resp);

		List<POCreationDTO> DbPOS = pOCreationService.getPOData();

		for (int i = DbPOS.size(); i > DbPOS.size(); i--) {
			assertEquals("Suceess,If Order_value Match", "4.0", DbPOS.get(i).getOrder_value().toString());
		}
	}

}
