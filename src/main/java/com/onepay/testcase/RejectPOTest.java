package com.onepay.testcase;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.junit4.SpringRunner;

import com.onepay.model.POCreationDTO;
import com.onepay.service.RejectPOService;

import io.restassured.response.Response;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;

 
@RunWith(SpringRunner.class)
@SpringBootTest
class RejectPOTest {

	int poId = 0;
	String WrongpoId = "12";
	
	@Autowired
	RejectPOService rejectPOService;
	
	@BeforeEach
	public void initEach() {
		poId = rejectPOService.getData();
		System.out.println("**********GetRandomTestPOData*****poId.."+poId+"*******");
		assertNotEquals(0, poId);
		
	}
 
	//TestCase 1 : Check PO Approved or Not in DB
		@Test
		public void CheckPoRejected() {
			
			Response response = given().contentType("application/json").accept("application/json")
					.queryParam("po_id", poId).queryParam("RejectBy", "Admin").when()
					.post("http://localhost:8081" + "/RejectPO").then().contentType("").extract().response();

			// Now let us print the body of the message to see what response
			// we have received from the server
			System.out.println(response.getBody().asString());
			String resp = response.getBody().asString();
			
			assertEquals("Success, If Rejected Msg. is Matched", "Rejected", resp);
			
			ArrayList<POCreationDTO> poDbList = rejectPOService.getPOData(String.valueOf(poId));
	 
			assertEquals("POid Same then Data Inserted Succesfully", poId, poDbList.get(0).getPo_id());
		}
	
	//TestCase 2 : Check PO Approved Status is Updated or Not
	@Test
	public void CheckPoRejectedStatus() {
		
		Response response = given().contentType("application/json").accept("application/json")
				.queryParam("po_id", poId).queryParam("RejectBy", "Admin").when()
				.post("http://localhost:8081" + "/RejectPO").then().contentType("").extract().response();

		// Now let us print the body of the message to see what response
		// we have received from the server
		System.out.println(response.getBody().asString());
		String resp = response.getBody().asString();
		
		assertEquals("Success, If Rejected Msg. is Matched", "Rejected", resp);

		ArrayList<POCreationDTO> poDbList = rejectPOService.getPOData(String.valueOf(poId));
       
		//Default Status is 0, If Approved Status changed to 1 
		assertEquals("Status 2, Data Updated Successfully ", 2, poDbList.get(0).getPo_status());
	}
	 
	//TestCase 4 : Check PO LastUpdatedBy is Updated or Not
	@Test
	public void CheckPoRejectedLastUpdatedBy() {
		
		Response response = given().contentType("application/json").accept("application/json")
				.queryParam("po_id", poId).queryParam("RejectBy", "Admin").when()
				.post("http://localhost:8081" + "/RejectPO").then().contentType("").extract().response();

		// Now let us print the body of the message to see what response
		// we have received from the server
		System.out.println(response.getBody().asString());
		String resp = response.getBody().asString();
		
		assertEquals("Success, If Rejected Msg. is Matched", "Rejected", resp);

		ArrayList<POCreationDTO> poDbList = rejectPOService.getPOData(String.valueOf(poId));
       
		//Default Status is 0, If Approved Status changed to 1 
		assertEquals("LastUpdatedBY Admin, Data Updated Successfully ", "Admin", poDbList.get(0).getLast_updated_by());
	}
 
	//TestCase 5 : Check For Wrong POId, PO is Approved or Not
	@Test
	public void CheckPoRejectedByWrongId() {
		
		Response response = given().contentType("application/json").accept("application/json")
				.queryParam("po_id", WrongpoId).queryParam("RejectBy", "Admin").when()
				.post("http://localhost:8081" + "/RejectPO").then().contentType("").extract().response();

		// Now let us print the body of the message to see what response
		// we have received from the server
		System.out.println(response.getBody().asString());
		String resp = response.getBody().asString();
 
		assertEquals("Success, If Not Rejected Msg. is Matched", "Not Rejected ! PO is not there for Id "+WrongpoId, resp);

		ArrayList<POCreationDTO> poDbList = rejectPOService.getPOData(WrongpoId);
		 
		assertNull(poDbList); 
 
	}
 
	

}
