package com.onepay.service;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;

import com.onepay.model.POCreationDTO;
import com.onepay.repository.ApprovePORepository;
import org.springframework.stereotype.Service;

@Service
public class ApprovePOService {

	@Autowired
	ApprovePORepository approvePORepository;
	

	public boolean approvPO(String po_id, java.util.Date poDate, String approvedBy) {
		int poId = Integer.valueOf(po_id);
		try {
			ArrayList<POCreationDTO> listPO = approvePORepository.findByPoId(poId);
			if (!listPO.isEmpty()) {
				approvePORepository.updateApproveStatus(1,approvedBy, poDate, poDate, approvedBy, poId);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}
	
	public ArrayList<POCreationDTO> getPOData(String po_id) {
		int poId = Integer.valueOf(po_id);
		try {
			ArrayList<POCreationDTO> listPO = approvePORepository.findByPoId(poId);
			if (!listPO.isEmpty()) {
 				return listPO;
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	
	public int getData() {
		 
		ArrayList<POCreationDTO> PoList = (ArrayList<POCreationDTO>) approvePORepository.findAll() ;

		if (PoList.isEmpty()) {
			return 0;
		} else {
			int poId = PoList.get(0).getPo_id();
			return poId;
		}
	}


}
