package com.onepay.service;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onepay.model.POCreationDTO;
import com.onepay.repository.RejectPORepository;


@Service
public class RejectPOService {
	
	@Autowired
	RejectPORepository rejectPORepository;


	public boolean rejectPO(String po_id, Date poDate, String rejectBy) {
		int poId = Integer.valueOf(po_id);
		try {
			ArrayList<POCreationDTO> listPO = rejectPORepository.findByPoId(poId);
			if (!listPO.isEmpty()) {
				rejectPORepository.updateRejectStatus(2,poDate, rejectBy, poId);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}
	
	public ArrayList<POCreationDTO> getPOData(String po_id) {
		int poId = Integer.valueOf(po_id);
		try {
			ArrayList<POCreationDTO> listPO = rejectPORepository.findByPoId(poId);
			if (!listPO.isEmpty()) {
 				return listPO;
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	
	public int getData() {
		 
		ArrayList<POCreationDTO> PoList = (ArrayList<POCreationDTO>) rejectPORepository.findAll() ;

		if (PoList.isEmpty()) {
			return 0;
		} else {
			int poId = PoList.get(0).getPo_id();
			return poId;
		}
	}

}
