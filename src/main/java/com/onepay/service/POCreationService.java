package com.onepay.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onepay.model.POCreationDTO;
import com.onepay.repository.CreatePORepository;

@Service
public class POCreationService {

	private Double OrderValue = null;
	private int POId = 0;

	@Autowired
	CreatePORepository createPORepository;

	public String createSinglePO(Date fromdate, String supplierId, String tagClassId, String orderQuantity,
			String unitPrice) {

		POCreationDTO pOCreationModel = new POCreationDTO();
		try {
			OrderValue = Double.valueOf(Integer.valueOf(orderQuantity) * Integer.valueOf(unitPrice));
			POId = RandomUtils.nextInt(1, 52 + 1);
			System.out.println("PoId" + POId);
			pOCreationModel.setPo_id(POId);
			pOCreationModel.setSupp_id(supplierId);
			pOCreationModel.setPo_date(fromdate);
			pOCreationModel.setOrder_value(OrderValue);
			pOCreationModel.setCreated_by("Admin");
			pOCreationModel.setCreated_on(new Date());
			pOCreationModel.setTotal_order_value(OrderValue);

			createPORepository.save(pOCreationModel);

			return "Success POId" + POId + "";
			// TODO Auto-generated method stub
		} catch (Exception e) {
			return "Failure";
		}
	}
	
	public String createMultiplePO(Date fromdate, List<String> list1, List<String> list2, List<String> list3,
			List<String> list4) {

		POCreationDTO pOCreationModel = new POCreationDTO();
		try {
			List<Integer> poList = new ArrayList<Integer>();
			List<Double> ttlValList = new ArrayList<Double>();
			double sum = 0;
			for (int i = 0; i < list1.size(); i++) {

				OrderValue = Double.valueOf(Integer.valueOf(list3.get(i)) * Integer.valueOf(list4.get(i)));
				ttlValList.add(OrderValue);
				POId = RandomUtils.nextInt(1,52 + 1);
				pOCreationModel.setPo_id(POId);
				pOCreationModel.setSupp_id(list1.get(i));
				pOCreationModel.setPo_date(fromdate);
				pOCreationModel.setOrder_value(Double.valueOf(list4.get(i)));
				pOCreationModel.setTotal_order_value(OrderValue);
				pOCreationModel.setCreated_by("Admin");
				pOCreationModel.setCreated_on(new Date());
				poList.add(pOCreationModel.getPo_id());

				createPORepository.save(pOCreationModel);
			}

			for (Double d : ttlValList) {
				sum += d;
			}
			return "Success POId*" + poList + "*totalValue*" + sum;
 

		} catch (Exception e) {
			return "Failure";
		}
	}
	
	public int getData(int poId1) {
 
		ArrayList<POCreationDTO> PoList = createPORepository.findByPoId(poId1);

		if (PoList.isEmpty()) {
			return 0;
		} else {
			int poId = PoList.get(0).getPo_id();
			return poId;
		}
	}

	public void ResetData() {

		createPORepository.deleteAll();
	}

	public int findAllData() {

		List<POCreationDTO> poLists =(List<POCreationDTO>) createPORepository.findAll(); 
		if(!poLists.isEmpty()) {
		int j = poLists.size();
		return j;
		}
		else {
			return 0;	
		}
	}
	
	public List<Integer> findPoId() {

		List<POCreationDTO> poLists =(List<POCreationDTO>) createPORepository.findAll(); 
		if(!poLists.isEmpty()) {
			List<Integer> poList = new ArrayList<Integer>();
			for(int i=0;i<poLists.size();i++) {
				poList.add(poLists.get(i).getPo_id() );
			}
			return poList;
		 
		}
		else {
			return null;	
		}
	}
	
	public ArrayList<POCreationDTO> getPOData( ) {
 		try {
			ArrayList<POCreationDTO> poLists = (ArrayList<POCreationDTO>) createPORepository.findAll(); 
			if (!poLists.isEmpty()) {
 				return poLists;
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	

	public Double getPOttlQty( ) {
 		try {
 			int poSizeDb =  findAllData();
 			
 			ArrayList<POCreationDTO> poListDb =  getPOData();
 			Double qtysum =0.0;
 			for(int i = 0;i<poSizeDb; i++) {
 				Double val = poListDb.get(i).getTotal_order_value();
 				qtysum += val;
 			}
 			return qtysum;
		} catch (Exception e) {
			e.printStackTrace();
			return 0.0;
		}
		

	}
	



}
